	
json.products @products do |product|
	if product.user_id == current_user.id  	
	  json.product_id product.id
	  json.name product.name
	  json.price product.price
	  json.user current_user.email
	  json.user_id product.user_id 	
	end
end	

